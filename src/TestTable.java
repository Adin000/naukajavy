import java.util.LinkedList;

public class TestTable {
	public static void main(String[] args) {

		Object[] table;

		LinkedList<Object> list_1 = new LinkedList<>();

		LinkedList<Object[]> list_2 = new LinkedList<>();

		list_1.add("Emilia");
		list_1.add("Adi");

		list_2.add(list_1.toArray()); // 0
		list_2.add(list_1.toArray()); // 1
		list_1.add("Bartłomiej");
		list_2.add(list_1.toArray()); // 2

		table = list_2.toArray();

	}
}
