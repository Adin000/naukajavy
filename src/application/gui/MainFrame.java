package application.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import application.Repository;
import application.data.Address;

public class MainFrame extends JFrame {

	private TableModel tableModel;
	private JTable jTable;
	private JScrollPane scrollPane;

	private JPanel tablePanel;
	private JPanel buttonPanel;

	private Repository repository;

	private JButton compButton;
	private JButton emplButton;
	private JButton addressButton;
	private JButton addButton;
	private JTextField nameField;
	private JTextField cityField;
	private JTextField countryField;

	public MainFrame() {
		repository = new Repository();
		setLayout(new BorderLayout());
		setTitle("Najbrzydsza apka �wiata :(");

		tablePanel = new JPanel();
		buttonPanel = new JPanel();

		initializeButtonPanel();
		setCompaniesInModelTable();
		initializeTable();
		setBounds(300, 200, 700, 300);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void initializeTable() {
		jTable = new JTable(tableModel);
		jTable.setFillsViewportHeight(true);
		scrollPane = new JScrollPane(jTable);
		add(scrollPane, BorderLayout.WEST);
	}

	private void initializeButtonPanel() {
		compButton = new JButton("COMP");
		emplButton = new JButton("EMPL");
		addressButton = new JButton("ADDRESS");
		initializeAddButton();
		nameField = new JTextField("Name");
		cityField = new JTextField("City");
		countryField = new JTextField("Country");

		buttonPanel.setLayout(new GridLayout(0, 1, 2, 0)); // brzydko XD
		buttonPanel.add(compButton);
		buttonPanel.add(emplButton);
		buttonPanel.add(addressButton);
		buttonPanel.add(nameField);
		buttonPanel.add(cityField);
		buttonPanel.add(countryField);
		buttonPanel.add(addButton);
		add(buttonPanel, BorderLayout.EAST);
	}

	private void initializeAddButton() {
		addButton = new JButton("ADD");
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String country = countryField.getText();
				String name = nameField.getText();
				String city = cityField.getText();

				repository.addCompany(name, new Address(city, country));
				setCompaniesInModelTable();
				jTable.removeAll();
				jTable.setModel(tableModel);
			}
		});
	}

	public void setCompaniesInModelTable() {
		String[] columnNames = { "Nazwa Firmy", "Adres", "Liczba Pracownik�w" };
		Object[][] data = repository.getAllCompanies(); // OBCZAJ TO!!!!!!!!
		tableModel = new TableModel(columnNames, data);
	}

	public static void main(String[] args) {
		new MainFrame();
	}
}
