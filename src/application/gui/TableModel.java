package application.gui;

import javax.swing.table.AbstractTableModel;

import application.Repository;

public class TableModel extends AbstractTableModel {

	private String[] columnNames;
	private Object[][] data;
	
	//Repository repoz = new Repository();


	public TableModel() {

	}

	public TableModel(String[] columnNames, Object[][] data) {
		this.columnNames = columnNames;
		this.data = data;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return data.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
}
