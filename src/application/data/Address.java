package application.data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Address {

	private String city;
	private String country;
	private Set<Company> companies;

	public Address(String city, String country) {
		this.city = city;
		this.country = country;
		this.companies = new HashSet<>();
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "[" + city + ", " + country + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		return true;
	}
	
	public Set<Company> getCompanies() {
		return companies;
	}
	
	public void setCompanies(Set<Company> companies){
		this.companies = companies;
	}
	
	public boolean addCompanyToAddressSet(Company company){			// pojebane 2/2
		return companies.add(company);
	}
	
	public void getCompaniesSet(){
		companies.forEach(System.out::println);
	}
}
