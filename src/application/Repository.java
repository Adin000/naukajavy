package application;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import application.data.Address;
import application.data.Company;
import application.data.Employee;

public class Repository {

	private Map<String, Company> companyTable;
	private Map<String, Employee> employeeTable;
	private Map<String, Address> addressTable;

	public Repository() {
		companyTable = new HashMap<>();
		employeeTable = new HashMap<>();
		addressTable = new HashMap<>();
		initialize();
	}

	public void initialize() {
		Address adr = new Address("Stambul", "Turcja");
		addCompany("Arbuzy", adr);
		addCompany("Granaty", adr);
		addCompany("Morele", adr);

		adr = new Address("Berlin", "Niemcy");
		addCompany("Ceg�y", adr);
		addCompany("Banany", adr);
		addCompany("Wi�nie", adr);

		addEmplooyeToCompany("Arbuzy", new Employee("Emi", "Rogal"));
	}

	public boolean addCompany(String name, Address address) {
		Company company = new Company(name, address);
		if (companyTable.put(name, company) == null) {
			address.addCompanyToAddressSet(company); // pojebane 1/2
			return true;
		}
		return false;
	}

	public boolean addEmplooyeToCompany(String companyName, Employee employee) {
		Company company = companyTable.get(companyName);
		if (company == null) {
			return false;
		}
		String key = employee.getName() + employee.getSurname();
		Employee storedEmployee = employeeTable.get(key);

		if (storedEmployee != null) {
			if (storedEmployee.getCompany() != null) {
				return false;
			} else {
				storedEmployee.setCompany(company);
				company.getEmployees().add(storedEmployee);
			}
		}

		Set<Employee> set = company.getEmployees();

		if (set.add(employee)) {
			employee.setCompany(company);
			return true;
		}
		return false;
	}

	public boolean addEmployee(String name, String surname) {
		Employee employee = new Employee(name, surname);
		String key = name + surname;
		return employeeTable.put(key, employee) != null;
	}

	public void printCompanyAddress(String nazwa) {
		System.out.println("Adres firmy " + nazwa + " to: " + companyTable.get(nazwa).getAddress());
	}

	public String printCompanies(Address address) {
		return address.getCompanies().toString();
	}

	// ma zwracac tablice String�w jak w klasie MainFrame
	// Tablica Stringow powinna zawierac nazwe, address (rozbity na city i
	// country) oraz liczbe pracownikow (jako String).
	public Object[][] getAllCompanies() {
		int rowNumber = companyTable.size();

		Object[][] data = new Object[rowNumber][];
		Collection<Company> companies = companyTable.values();
		List<Object> row = new LinkedList<>();

		int i = 0;
		for (Company company : companies) {
			row.add(company.getName());
			row.add(company.getAddress());
			row.add(company.getEmployees().size());

			System.out.println(company.getName() + " " + company.getAddress() + " " + company.getEmployees().size());
			data[i] = row.toArray();
			++i;
			row.clear();
		}
		return data;
	}

	// ma zwracac tablice String�w jak w klasie MainFrame
	// ktorych nazwa zaczyna si� od "name"
	public String[][] getCompaniesByName(String name) {

		// tworze wyrazenie regularne - '^' m�wi:
		// szukam czegos, co zaczyna sie od tego co jest
		// dalej
		// tutaj lap tutorial:
		// http://www.tutorialspoint.com/java/java_regular_expressions.htm
		// moze to byc nieco troche zrobione (.compile) ale ogolnie zasada
		// jest ta sama.
		String regex = "^" + name;
		boolean result = Pattern.matches(regex, "NazwaFirmy");

		// Testujesz tutaj kazda Company, czy jej name spelnia warunki
		// i jesli tak to wrzucac do zwracanej tablicy. Robisz to w petli.
		// Aby otrzymac ladna kolekcje Company, uzyj na HashSet metody
		// .values() - zwraca ona Kolekcje Company. Mozesz tez pobrac
		// same klucze za pomoca innej metody :)
		// Jak masz kolekcje Company, to mozesz przegl�da� j�
		// p�tl� foreach:

		for (Company company : companyTable.values()) {
			// company.wywolanieMetody();
		}

		return new String[0][0];
		// dor�b prosze podobne wyszukiwanie do pracownika do
		// jego imienia, nazwiska, do firmy do jej adresu (miasta i kraju).
		// czyli dojda jakies 4 metody :P

		// mozesz zmienic znaczenie wyszukiwania z "zaczyna sie"
		// na "zawiera w sobie", ale to musisz troche o regex poczytac
	}

	public int employeesCount(String name) { // companyName
		return companyTable.get(name).emplCount();
	}
}
